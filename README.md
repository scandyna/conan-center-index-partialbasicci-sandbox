
# conan-center-index partial CI sandbox

When contributing to [conan-center-index](https://github.com/conan-io/conan-center-index),
it is required to test the recipe locally for at least one configuration.

The test must be done with the conan-center hook.

This is explained in the [Developing Recipes Locally](https://github.com/conan-io/conan-center-index/blob/master/docs/developing_recipes_locally.md) documentation.

To avoid altering my personal PC, and try to automate the thing a little bit,
I created this sandbox.

If course, I can't build all the Conan recipes
(try not too abuse GitLAB-CI shared runners to much + Windows runners cost me a lot).

# How to do it ?

Edit the `.gitlab_ci.yml` and add, uncomment, comment jobs.
(yes, this is a sandbox project).

# Documentation

* https://github.com/conan-io/conan-center-index/blob/master/CONTRIBUTING.md
