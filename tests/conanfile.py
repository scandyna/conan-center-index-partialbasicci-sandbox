from conans import ConanFile, tools
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake
import os

class TestQtBuildsConan(ConanFile):
  name = "TestQtBuilds"
  version = "0.1"
  license = "BSD 3-Clause"
  url = "https://gitlab.com/scandyna/conan-center-index-partialbasicci-sandbox"
  description = "Just to test some Qt builds"
  settings = "os", "compiler", "build_type", "arch"
  options = {"shared": [True, False]}
  default_options = {"shared": True}
  generators = "CMakeDeps", "CMakeToolchain", "VirtualBuildEnv"

  def requirements(self):
    self.requires("qt/5.15.6")

  # When using --profile:build xx and --profile:host xx ,
  # the dependencies declared in build_requires and tool_requires
  # will not generate the required files.
  # see:
  # - https://github.com/conan-io/conan/issues/10272
  # - https://github.com/conan-io/conan/issues/9951
  def build_requirements(self):
    # TODO fix once issue solved
    # Due to a issue using GitLab Conan repository,
    # version ranges are not possible.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/333638
    self.tool_requires("MdtCMakeModules/0.18.3@scandyna/testing", force_host_context=True)
